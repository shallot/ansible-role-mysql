# Copyright (c) 2018-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://www.vagrantup.com/docs/vagrantfile/vagrant_version.html
Vagrant.require_version '>= 2.0.0'

# https://docs.ansible.com/ansible/latest/reference_appendices/config.html#the-configuration-file
ENV['ANSIBLE_CONFIG'] ||= File.join(__dir__, '.ansible.cfg')

# https://www.vagrantup.com/docs/vagrantfile/version.html
Vagrant.configure(2) do |vagrant|

  # https://www.vagrantup.com/docs/synced-folders/basic_usage.html#disabling
  vagrant.vm.synced_folder('.', '/vagrant', disabled: true)

  # https://www.vagrantup.com/docs/multi-machine/
  vagrant.vm.define('debian-stretch') do |config|
    # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
    config.vm.box = 'debian/stretch64'
    config.vm.hostname = 'debian-stretch.test'
  end

  # https://wiki.centos.org/Manuals/ReleaseNotes/CentOS7
  vagrant.vm.define('centos-7') do |config|
    config.vm.box = 'centos/7'
    config.vm.hostname = 'centos7-64.test'
  end

  # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
  vagrant.vm.box_check_update = false
  vagrant.vm.post_up_message = nil

  # https://www.vagrantup.com/docs/provisioning/ansible.html
  vagrant.vm.provision('ansible') do |ansible|
    ansible.compatibility_mode = '2.0'
    ansible.playbook = File.join(__dir__, 'playbook.yml')
  end

end
